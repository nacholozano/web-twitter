WebTwitter
========================
  
Implement a web-based social networking application (similar to Twitter) satisfying the scenarios below.

**NOTE**: Please it is important that you add instructions about how to run the application.
 
 
<h2>Scenarios</h2> 

**Posting a message**
-  As a user we should be able to add new messages to the timeline 

**Following/Unfollowing a user**
- As a user we should be able to follow or unfollow a user.

**View timeline messages** 
-  Once the main view is accessed, the aggregated list of all messages from users we follow (including mine) will be displayed.
-  When selecting one of the following users, the timeline should only show posts from that user.
-  Again, if no follower is selected, it should show the aggregated message list of all the users you follow.
-  The content of a message should include this info: author name, date, content

<h2>How the UI should look like?</h2>

You are not intended to be proven as a designer. The provided mockup are just a guideline, feel free to build whatever you want. You can use a component's library in order to help you to build the UI or code it from scratch, no preference here.


![View the wall](resources/wireframe.png)
 
 
 

