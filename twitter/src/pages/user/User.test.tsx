import { TweetsService } from "../../service/tweets/tweets.service";
import { UsersService } from "../../service/users/users.service";
import { UserComponent } from "./User"
import { render, screen } from '@testing-library/react';
import { TimelineProps } from "../../components/timeline/Timeline.model";

jest.mock('react-router-dom', () => ({
  useParams: () => '2',
  Link: () => (<></>)
}));

jest.mock('../../components/timeline/Timeline', () => ({
  Timeline: (props: TimelineProps) => <div data-testid='timeline-content'>{props.tweets}</div>
}));

describe('User page', () => {

  let userService: UsersService;

  beforeEach(() => {
    userService = new UsersService([], new TweetsService([]));
  })

  test('load user tweets', () => {

    userService.getUserTweets = jest.fn();
    userService.timelineToView = jest.fn().mockReturnValue('tweets');

    render(<UserComponent usersService={userService}/>)

    const timeline = screen.getByTestId('timeline-content');

    expect(timeline.textContent).toBe('tweets');
  })

})