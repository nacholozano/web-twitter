import { useState } from "react";
import { Link, useParams } from "react-router-dom";
import { Timeline } from "../../components/timeline/Timeline";
import { UsersService, usersService } from "../../service/users/users.service";

interface componentDI {
  usersService: UsersService
}

export function UserComponent(props: componentDI) {

  let { id } = useParams();

  const [tweets] = useState(
    props.usersService.timelineToView(
      props.usersService.getUserTweets(id)
    )
  );

  return (
    <div>
      <div>
        <Link to="/">Go home</Link>
      </div>
      <Timeline tweets={tweets}/>
    </div>
  )
}

export function User() {
  return <UserComponent usersService={usersService}/>
}