import { fireEvent, render, screen } from "@testing-library/react"
import { FollowListProps } from "../../components/follow-list/FollowList.model";
import { NewTweetProps } from "../../components/new-tweet/NewTweet.model";
import { TimelineProps } from "../../components/timeline/Timeline.model";
import { TweetsService } from "../../service/tweets/tweets.service";
import { UsersService } from "../../service/users/users.service"
import { HomeComponent } from "./Home";

jest.mock('../../components/follow-list/FollowList', () => ({
  FollowList: (props: FollowListProps) => <>
    <div data-testid='follow-list-content'>{props.list}</div>,
    <button data-testid='follow-list-button' onClick={() => props.onAction('') }></button>
  </>
}));

jest.mock('../../components/timeline/Timeline', () => ({
  Timeline: (props: TimelineProps) => <div data-testid='timeline-content'>{props.tweets}</div>
}));

jest.mock('../../components/new-tweet/NewTweet', () => ({
  NewTweet: (props: NewTweetProps) => <button data-testid='submit-tweet' onClick={() => props.onTweetSubmit('') }></button>
}));

describe('Home page', () => {

  let usersService: UsersService;
  let tweetsService: TweetsService;

  beforeEach(() => {
    tweetsService = new TweetsService([]);
    usersService = new UsersService([], tweetsService);
  })

  test('init empty status', () => {

    usersService.getFollowingUsers = jest.fn().mockReturnValue('');
    usersService.getNotFollowingUsers = jest.fn().mockReturnValue('1,2,3');

    usersService.getTimeline = jest.fn();
    usersService.timelineToView = jest.fn().mockReturnValue('');

    render(<HomeComponent usersService={usersService} tweetsService={tweetsService}/>)

    const followLists = screen.getAllByTestId('follow-list-content');
    const timeline = screen.getByTestId('timeline-content');

    expect(followLists[0].textContent).toBe('');
    expect(followLists[1].textContent).toBe('1,2,3');
    expect(timeline.textContent).toBe('');
  })

  test('Follow user', () => {

    usersService.follow = jest.fn();

    usersService.getFollowingUsers = jest.fn().mockReturnValue('')
      .mockReturnValueOnce('')
      .mockReturnValueOnce('1');

    usersService.getNotFollowingUsers = jest.fn().mockReturnValue('')
      .mockReturnValueOnce('1,2,3')
      .mockReturnValueOnce('2,3');

    usersService.getTimeline = jest.fn();
    usersService.timelineToView = jest.fn().mockReturnValue('')
      .mockReturnValueOnce('')
      .mockReturnValueOnce('1,4,6');

    render(<HomeComponent usersService={usersService} tweetsService={tweetsService}/>)

    const followLists = screen.getAllByTestId('follow-list-content');
    const followListsButton = screen.getAllByTestId('follow-list-button');
    const timeline = screen.getByTestId('timeline-content');

    fireEvent.click(followListsButton[1]);

    expect(followLists[0].textContent).toBe('1');
    expect(followLists[1].textContent).toBe('2,3');
    expect(timeline.textContent).toBe('1,4,6');
  })

  test('Unfollow user', () => {
    usersService.removeFollowing = jest.fn();

    usersService.getFollowingUsers = jest.fn().mockReturnValue('')
      .mockReturnValueOnce('1')
      .mockReturnValueOnce('');

    usersService.getNotFollowingUsers = jest.fn().mockReturnValue('')
      .mockReturnValueOnce('2,3')
      .mockReturnValueOnce('1,2,3');

    usersService.getTimeline = jest.fn();
    usersService.timelineToView = jest.fn().mockReturnValue('')
      .mockReturnValueOnce('1,4,6')
      .mockReturnValueOnce('');

    render(<HomeComponent usersService={usersService} tweetsService={tweetsService}/>)

    const followLists = screen.getAllByTestId('follow-list-content');
    const followListsButton = screen.getAllByTestId('follow-list-button');
    const timeline = screen.getByTestId('timeline-content');

    fireEvent.click(followListsButton[0]);

    expect(followLists[0].textContent).toBe('');
    expect(followLists[1].textContent).toBe('1,2,3');
    expect(timeline.textContent).toBe('');
  })

  test('Submit tweet', () => {
    
    usersService.getFollowingUsers = jest.fn();
    usersService.getNotFollowingUsers = jest.fn();
    usersService.getUser = jest.fn();

    usersService.getTimeline = jest.fn();
    usersService.timelineToView = jest.fn().mockReturnValue('')
      .mockReturnValueOnce('')
      .mockReturnValueOnce('1')
      .mockReturnValueOnce('1,2');

    tweetsService.addTweet = jest.fn();

    render(<HomeComponent usersService={usersService} tweetsService={tweetsService}/>)

    const submitTweet = screen.getByTestId('submit-tweet');
    const timeline = screen.getByTestId('timeline-content');
    
    expect(timeline.textContent).toBe('');

    fireEvent.click(submitTweet);

    expect(timeline.textContent).toBe('1');

    fireEvent.click(submitTweet);

    expect(timeline.textContent).toBe('1,2');
  })

})