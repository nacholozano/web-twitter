import { useEffect, useState } from "react";
import { FollowItem } from "../../components/follow-item/FollowItem";
import { FollowingItem } from "../../components/following-item/FollowingItem";
import { FollowList } from "../../components/follow-list/FollowList";
import { NewTweet } from "../../components/new-tweet/NewTweet";
import { Timeline } from "../../components/timeline/Timeline";
import { User, UserId } from "../../model/User.model";
import { TweetsService, tweetsService } from "../../service/tweets/tweets.service";
import { UsersService, usersService } from "../../service/users/users.service";
import styles from './Home.module.css';
import { TweetProps } from "../../components/tweet/Tweet.model";

interface componentDI {
  usersService: UsersService;
  tweetsService: TweetsService;
}

export function HomeComponent(props: componentDI) {

  const userId = props.usersService.getCurrentUser();

  const [followingList, setFollowingList]: [User[], (list: User[]) => void] = useState([]);
  const [notFollowingList, setNotFollowingList]: [User[], (list: User[]) => void] = useState([]);
  const [timeline, setTimeline]: [TweetProps[], (list: TweetProps[]) => void ] = useState([]);

  const handleUnfollow = (id: UserId) => {
    props.usersService.removeFollowing(userId, id);
    updateFollowersAndTimeline();
  }

  const handleFollow = (id: UserId) => {
    props.usersService.follow(userId, id);
    updateFollowersAndTimeline();
  }

  const updateFollowersAndTimeline = () => {
    setFollowingList( props.usersService.getFollowingUsers(userId) );
    setNotFollowingList( props.usersService.getNotFollowingUsers(userId) );
    setTimeline( 
      props.usersService.timelineToView(
        props.usersService.getTimeline(userId)
      ) 
    );
  }

  const handleTweetSubmit = (text: string) => {
    props.tweetsService.addTweet(
      props.usersService.getUser(userId),
      text
    )

    updateFollowersAndTimeline();
  }

  useEffect(() => {
    updateFollowersAndTimeline();
  }, [])

  return (
    <div className={styles.pageContainer}>
      <div className={styles.followListContainer}>

        <div className={styles.followingList}>
          <FollowList 
            title='Following' 
            componentItem={FollowingItem} 
            list={followingList} 
            onAction={handleUnfollow}
          />
        </div>

        <div className={styles.followingList}>
          <FollowList 
            title='Follow' 
            componentItem={FollowItem} 
            list={notFollowingList} 
            onAction={handleFollow}
          />
        </div>

      </div>
      <div className={styles.tweetsContainer}>
        <Timeline tweets={timeline}/>

        <div className={styles.newTweetContainer}>
          <NewTweet onTweetSubmit={handleTweetSubmit}/>
        </div>
      </div>
    </div>
  );
}

export function Home() {
  return <HomeComponent usersService={usersService} tweetsService={tweetsService}/>
}