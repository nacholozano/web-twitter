import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import styles from './App.module.css';
import { Home } from "./pages/home/Home";
import { User } from "./pages/user/User";

function App() {
  return (
    <div className={styles.routesContainer}>
      <Router>
        <Routes>

          <Route path="user/:id" element={<User/>} />
          <Route path="/" element={<Home/>} />
          
        </Routes>
      </Router>
    </div>
  );
}

export default App;
