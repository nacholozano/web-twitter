import { FollowItemProps } from "./FollowItem.model";
import styles from './FollowItem.module.css';

export function FollowItem(props: FollowItemProps) {

  const handleFollow = () => {
    props.onAction(props.id)
  }

  return (
    <div className={styles.itemContainer}>
      <span data-testid='user-name'>{props.name}</span>
      <button onClick={handleFollow} data-testid='follow'>Follow</button>
    </div>
  );
}