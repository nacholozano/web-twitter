import { fireEvent, render, screen } from '@testing-library/react';
import { FollowItem } from './FollowItem';

describe('Follow item component', () => {

  test('renders user name', () => {
    const name = 'Alice';

    render(<FollowItem name={name} id='1' onAction={null}/>);

    const userName = screen.getByTestId('user-name');
    
    expect(userName.textContent).toBe(name);
  });

  test('call follow', () => {
    const onAction = jest.fn();

    render(<FollowItem name='' id='1' onAction={onAction}/>);

    fireEvent.click(screen.getByTestId('follow'));

    expect(onAction).toHaveBeenCalledTimes(1);
    expect(onAction).toHaveBeenCalledWith('1');
  })

})