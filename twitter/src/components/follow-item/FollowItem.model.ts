import { UserId } from "../../model/User.model";

export interface FollowItemProps {
  name: string;
  id: UserId;
  onAction: (id: UserId) => void
}