import { TweetProps } from "./Tweet.model";
import styles from './Tweet.module.css';

export function Tweet(props: TweetProps) {
  return (
    <div className={styles.tweetContainer}>
      <div className={styles.headerContainer}>
        <div data-testid='user-name'>
          { props.userName }
        </div>
        <div data-testid='time-ago'>
          { props.timeAgo }
        </div>
      </div>
      
      <p data-testid='text'>
        { props.text }
      </p>
    </div>
  )
}
