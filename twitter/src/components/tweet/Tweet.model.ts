export interface TweetProps {
  userName: string;
  timeAgo: string;
  text: string;
}