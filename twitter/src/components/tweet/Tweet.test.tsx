import { render, screen } from '@testing-library/react';
import { Tweet } from './Tweet';

describe('Tweet component', () => {

  test('renders user name', () => {
    const name = 'Alice';

    render(<Tweet userName={name} timeAgo='' text=''/>);

    const userName = screen.getByTestId('user-name');
    
    expect(userName.textContent).toBe(name);
  });

  test('renders time', () => {
    const time = '21min';

    render(<Tweet userName='' timeAgo={time} text=''/>);

    const timeAgo = screen.getByTestId('time-ago');

    expect(timeAgo.textContent).toBe(time);
  });

  test('renders text', () => {
    const content = 'lorem ipsum';

    render(<Tweet userName='' timeAgo='' text={content}/>);
    
    const text = screen.getByTestId('text');

    expect(text.textContent).toBe(content);
  });

})
