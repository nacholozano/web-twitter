import { useState } from "react";
import { NewTweetProps } from "./NewTweet.model";
import styles from './NewTweet.module.css';

export function NewTweet(props: NewTweetProps) {

  const [tweetContent, setTweetContent] = useState('');

  const handleChange = (event: any) => {
    setTweetContent(event.target.value);
  }

  const handleSubmit = () => {
    setTweetContent('');
    props.onTweetSubmit(tweetContent)
  }

  return (
    <div>
      <p>Post a new message</p>
      <textarea className={styles.textInput} value={tweetContent} onChange={handleChange} data-testid="textarea"></textarea>
      <div className={styles.actionContainer}>
        <button onClick={handleSubmit} disabled={!tweetContent} data-testid="post" className={styles.submitButton}>Post</button>    
      </div>
    </div>
  )
}