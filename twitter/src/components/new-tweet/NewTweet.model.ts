export interface NewTweetProps {
  onTweetSubmit: (content: string) => void;
}