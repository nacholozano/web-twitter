import { fireEvent, render, screen } from '@testing-library/react';
import { NewTweet } from './NewTweet';

describe('New tweet component', () => {

  test('init state', () => {
    const onTweetSubmit = jest.fn();

    render(<NewTweet onTweetSubmit={onTweetSubmit}/>);

    const textarea = screen.getByTestId('textarea');
    const post = screen.getByTestId('post');

    expect(textarea).toHaveValue('');
    fireEvent.click(post);

    expect(onTweetSubmit).toHaveBeenCalledTimes(0);
  })

  test('send tweet', () => {
    const onTweetSubmit = jest.fn();

    render(<NewTweet onTweetSubmit={onTweetSubmit}/>);

    const textarea = screen.getByTestId('textarea');
    const post = screen.getByTestId('post');

    fireEvent.change(textarea, {target: {value: 'tweet'}});
    fireEvent.click(post);

    expect(onTweetSubmit).toHaveBeenCalledWith('tweet');
    expect(textarea).toHaveValue('');
  })

})