import { Link } from "react-router-dom";
import { FollowingItemProps } from "./FollowingItem.model";
import styles from './FollowingItem.module.css';

export function FollowingItem(props: FollowingItemProps) {

  const handleUnfollow = () => {
    props.onAction(props.id)
  }

  return (
    <div className={styles.itemContainer}>
      <Link to={'/user/' + props.id}>{props.name}</Link>
      <button onClick={handleUnfollow} data-testid='unfollow'>Unfollow</button>
    </div>
  );
}