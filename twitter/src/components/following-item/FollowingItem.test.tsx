import { fireEvent, render, screen } from '@testing-library/react';
import { FollowingItem } from './FollowingItem';

jest.mock('react-router-dom', () => ({
  Link: () => (<></>)
}));

describe('Following item component', () => {

  test('call unfollow', () => {
    const onAction = jest.fn();

    render(<FollowingItem name='' id='1' onAction={onAction}/>);

    fireEvent.click(screen.getByTestId('unfollow'));

    expect(onAction).toHaveBeenCalledTimes(1);
    expect(onAction).toHaveBeenCalledWith('1');
  })

})