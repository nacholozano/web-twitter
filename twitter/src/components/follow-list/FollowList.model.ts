import { User, UserId } from "../../model/User.model";

export interface FollowListProps {
  title: string;
  componentItem: any;
  list: User[];
  onAction: (id: UserId) => void;
}