import { render, screen } from '@testing-library/react';
import { useEffect } from 'react';
import { User } from '../../model/User.model';
import { FollowItemProps } from '../follow-item/FollowItem.model';
import { FollowList } from './FollowList';

function ComponentMock(props: FollowItemProps) {

  useEffect(() => {
    props.onAction(props.id);
  }, [])
    
  return <div></div>;
}

describe('Follow list component', () => {

  let users: User[];

  beforeEach(() => {
    users = [
      {
        id: '1',
        name: 'name 1',
        tweets: [],
        following: []
      },
      {
        id: '2',
        name: 'name 2',
        tweets: [],
        following: []
      }
    ];
  })

  test('show title', () => {
    const titleParam = 'awesome title';

    render(<FollowList title={titleParam} list={[]} onAction={null} componentItem={ComponentMock}/>);

    const titleRender = screen.getByTestId('title');
    expect(titleRender.textContent).toBe(titleParam);
  })

  test('empty list', () => {
    render(<FollowList title='' list={[]} onAction={() => {}} componentItem={ComponentMock}/>);

    const empty = screen.queryByTestId('empty-list');
    expect(empty).toBeTruthy();

    const items = screen.queryAllByTestId('list-item');
    expect(items.length).toBe(0);
  })

  test('render list', () => {
    render(<FollowList title='' list={users} onAction={() => {}} componentItem={ComponentMock}/>);

    const empty = screen.queryByTestId('empty-list');
    expect(empty).toBeFalsy();

    const items = screen.queryAllByTestId('list-item');
    expect(items.length).toBe(2);
  })

  test('run action', () => {
    const action = jest.fn();

    render(<FollowList title='' list={users} onAction={action} componentItem={ComponentMock}/>);

    expect(action).toHaveBeenCalledWith('1');
    expect(action).toHaveBeenCalledWith('2');
    expect(action).toHaveBeenCalledTimes(2);
  })

})