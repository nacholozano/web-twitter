import { User, UserId } from "../../model/User.model";
import { FollowListProps } from "./FollowList.model";
import styles from './FollowList.module.css';

export function FollowList(props: FollowListProps) {

  const handleAction = (id: UserId) => {
    props.onAction(id);
  }

  return (
    <div>
      <p data-testid='title'>{props.title}</p>
      {
        props.list?.length 
        ? props.list.map((user: User, index: number) => {
          return (
            <div className={styles.itemContainer} key={user.id} data-testid='list-item'>
              <props.componentItem name={user.name} id={user.id} onAction={handleAction}/>
            </div>
          )
        })
        : <p className={styles.listEmptyMessage} data-testid='empty-list'>List is empty</p>
      }
    </div>
  )
}