import { TweetProps } from "../tweet/Tweet.model";

export interface TimelineProps {
  tweets: TweetProps[];
}