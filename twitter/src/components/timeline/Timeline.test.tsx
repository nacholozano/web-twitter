import { render, screen } from '@testing-library/react';
import { TweetProps } from '../tweet/Tweet.model';
import { Timeline } from './Timeline';

jest.mock('../tweet/Tweet', () => ({ Tweet: (props: TweetProps) => {
  return <div data-testid='tweet-mock'>{`${props.userName}-${props.timeAgo}-${props.text}`}</div>;
}}));

describe('Tweet component', () => {

  let timelineTweets: TweetProps[];

  beforeEach(() => {
    timelineTweets = [
      {
        userName: 'name1',
        timeAgo: 'time1',
        text: 'text1'
      },
      {
        userName: 'name2',
        timeAgo: 'time2',
        text: 'text2'
      },
      {
        userName: 'name3',
        timeAgo: 'time3',
        text: 'text4'
      }
    ]
  })

  test('renders complete timeline', () => {
    
    render(<Timeline tweets={timelineTweets}/>);

    const tweets = screen.queryAllByTestId('tweet-mock');
    
    expect(tweets.length).toBe(3);
  });

  test('pass params to Tweet component', () => {

    render(<Timeline tweets={timelineTweets}/>);

    const tweets = screen.queryAllByTestId('tweet-mock');
    
    expect(tweets[0].textContent).toBe('name1-time1-text1');
    expect(tweets[1].textContent).toBe('name2-time2-text2');
  })

})