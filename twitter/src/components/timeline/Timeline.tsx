import { Tweet } from "../tweet/Tweet";
import { TimelineProps } from "./Timeline.model";

export function Timeline(props: TimelineProps) {
  return (
    <div>
      <p>Timeline</p>
      {
        props.tweets.map((tweet, index) => <Tweet {...tweet} key={index}/> )
      }
    </div>
  )
}