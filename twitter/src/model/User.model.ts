import { TweetId } from "./Tweet.model";

export type UserId = string;

export interface User {
  id: UserId;
  name: string;
  tweets: TweetId[];
  following: UserId[];
}