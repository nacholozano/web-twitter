import { UserId } from "./User.model";

export type TweetId = string;

export interface Tweet {
  id: TweetId;
  userId: UserId;
  date: Date;
  text: string;
}