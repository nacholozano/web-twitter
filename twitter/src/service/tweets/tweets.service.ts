import { Tweet, TweetId } from "../../model/Tweet.model";
import { User } from "../../model/User.model";
import { tweets } from "./Tweets";

export class TweetsService {

  private _tweets: Tweet[];
  private _tweetIdCounter: number;

  constructor(tweets: Tweet[]) {
    this._tweets = tweets;
    this._tweetIdCounter = this._tweets.length ? Number(this._tweets[this._tweets.length -1].id) : 0;
  }

  getTweets(ids: TweetId[]): Tweet[] {
    return this._tweets
      .filter(tweet => ids.includes(tweet.id))
      .sort((prev, next) => next.date.getTime() - prev.date.getTime() );
  }

  addTweet(user: User, text: string) {
    this._tweetIdCounter++;
    const tweetId = this._tweetIdCounter + '';

    this._tweets.push({
      text,
      date: new Date(),
      userId: user.id,
      id: tweetId
    });

    user.tweets.push(tweetId);
  }

}

export const tweetsService = new TweetsService(tweets);