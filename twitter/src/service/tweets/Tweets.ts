import { Tweet } from "../../model/Tweet.model";

export const tweets: Tweet[] = [
  {
    id: '1',
    userId: '1',
    date: new Date(2022, 2, 1),
    text: 'Tweet 1'
  },
  {
    id: '2',
    userId: '2',
    date: new Date(2022, 2, 6, 18),
    text: 'Tweet 2'
  },
  {
    id: '3',
    userId: '1',
    date: new Date(2022, 1, 24),
    text: 'Tweet 3'
  },
  {
    id: '4',
    userId: '3',
    date: new Date(2021, 1, 10),
    text: 'Tweet 4'
  },
  {
    id: '5',
    userId: '2',
    date: new Date(2020, 1, 3),
    text: 'Tweet 5'
  }
];