import { Tweet } from "../../model/Tweet.model";
import { User } from "../../model/User.model";
import { TweetsService } from "./tweets.service";

describe('TweetsService', () => {

  let tweetsService: TweetsService;
  let tweets: Tweet[];

  beforeEach(() => {
    tweets = [
      {
        id: '1',
        userId: '1',
        date: new Date(2022, 2, 1),
        text: 'Tweet 1'
      },
      {
        id: '2',
        userId: '2',
        date: new Date(2022, 2, 6, 18),
        text: 'Tweet 2'
      }
    ]
    tweetsService = new TweetsService(tweets);
  })

  test('get tweet', () => {
    const tweets = tweetsService.getTweets(['2']);

    expect(tweets.length).toBe(1);
    expect(tweets[0].id).toBe('2');
  })

  test('get sorted tweets', () => {
    const tweets = tweetsService.getTweets(['1', '2']);

    expect(tweets.length).toBe(2);

    expect(tweets[0].id).toBe('2');
    expect(tweets[1].id).toBe('1');
  })

  test('add tweet', () => {
    const user: User = {
      id: '3',
      name: 'Bob',
      following: [],
      tweets: []
    };

    tweetsService.addTweet(user, 'First tweet');

    expect(user.tweets[0]).toBe('3')
    expect(tweetsService['_tweets'][2].text).toBe('First tweet');
  })

})

export {}