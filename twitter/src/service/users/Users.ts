import { User } from "../../model/User.model";

export const users: User[] = [
  {
    id: '1',
    name: 'name 1',
    tweets: ['1', '3'],
    following: ['2']
  },
  {
    id: '2',
    name: 'name 2',
    tweets: ['2', '5'],
    following: ['1', '3']
  },
  {
    id: '3',
    name: 'name 3',
    tweets: ['4'],
    following: []
  }
]