import { Tweet } from "../../model/Tweet.model";
import { User, UserId } from "../../model/User.model";
import { TweetsService, tweetsService } from "../tweets/tweets.service";
import { users } from "./Users";
import { TweetProps } from "../../components/tweet/Tweet.model";

export class UsersService {

  private _users: User[];
  private _tweetsService: TweetsService;
  private _currentUser = '2';
  private _months: string[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  constructor(users: User[], tweetsService: TweetsService) {
    this._users = users;
    this._tweetsService = tweetsService;
  }

  getCurrentUser(): UserId {
    return this._currentUser;
  }

  getUser(id: UserId): User {
    return this._users.find(user => user.id === id) || null;
  }

  getFollowingUsers(id: UserId): User[] {
    const userToSearch = this.getUser(id);

    return this._users.filter(user => userToSearch.following.includes(user.id) && user.id !== id )
  }

  getNotFollowingUsers(id: UserId): User[] {
    const userToSearch = this.getUser(id);

    return this._users.filter(user => !userToSearch.following.includes(user.id) && user.id !== id )
  }

  getTimeline(id: UserId): Tweet[] {
    const myTweets = this.getUser(id).tweets;

    const followingUsers: User[] = this.getFollowingUsers(id);
    const followingUsersTweets = followingUsers.map(user => user.tweets).reduce((prev, next) => [...prev, ...next] , [])

    const allTweets = [...myTweets, ...followingUsersTweets];

    return this._tweetsService.getTweets(allTweets);
  }

  getUserTweets(id: UserId): Tweet[] {
    const tweets = this.getUser(id).tweets;

    return this._tweetsService.getTweets(tweets);
  }

  removeFollowing(id: UserId, unfollowUserId: UserId) {
    const userIndex = this._users.findIndex(user => user.id === id);

    const followingToRemoveIndex = this._users[userIndex].following.findIndex(followingId => followingId === unfollowUserId);

    this._users[userIndex].following.splice(followingToRemoveIndex, 1);
  }

  follow(id: UserId, followUserId: UserId) {
    const userIndex = this._users.findIndex(user => user.id === id);

    this._users[userIndex].following.push(followUserId);
  }

  timelineToView(tweets: Tweet[]): TweetProps[] {
    return tweets.map(tweet => ({
      userName: this.getUser(tweet.userId).name,
      text: tweet.text,
      timeAgo: this._getTimeAgo(new Date(), tweet.date, this._months)
    }))
  }

  private _getTimeAgo(now: Date, date: Date, months: string[]) {
    let dateDiff: string = date.toString();

    const nowYear = now.getFullYear();
    const dateYear = date.getFullYear();

    if (nowYear === dateYear) {

      const nowMonth = now.getMonth();
      const dateMonth = date.getMonth();

      const nowDay = now.getDate();
      const dateDay = date.getDate();

      if (nowMonth === dateMonth && nowDay === dateDay) {

        const nowHours = now.getHours();
        const dateHours = date.getHours();

        if (nowHours === dateHours) {

          const nowMinutes = now.getMinutes();
          const dateMinutes = date.getMinutes();

          dateDiff = (nowMinutes - dateMinutes) + 'min';

        } else {
          dateDiff = (nowHours - dateHours) + 'h';
        }

      } else {
        dateDiff = dateDay + ' ' + months[dateMonth];
      }

    } else {
      dateDiff = months[date.getMonth()] + ' ' + dateYear.toString();
    }

    return dateDiff;
  }

}

export const usersService = new UsersService(users, tweetsService);