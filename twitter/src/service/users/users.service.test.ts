import { Tweet } from "../../model/Tweet.model";
import { User } from "../../model/User.model";
import { TweetsService } from "../tweets/tweets.service";
import { UsersService } from "./users.service";

describe('TweetsService', () => {

  let mockTweetsService: TweetsService;
  let usersService: UsersService;
  let users: User[];

  beforeEach(() => {

    mockTweetsService = new TweetsService([]);

    users = [
      {
        id: '1',
        name: 'name 1',
        tweets: ['1', '3'],
        following: ['2']
      },
      {
        id: '2',
        name: 'name 2',
        tweets: ['2', '5'],
        following: ['1', '3']
      },
      {
        id: '3',
        name: 'name 3',
        tweets: ['4'],
        following: []
      }
    ]

    usersService = new UsersService(users, mockTweetsService);

  })

  test('get user', () => {
    const user = usersService.getUser('2');

    expect(user).toBe(users[1]);
  })

  test('get following users', () => {
    const followingUsers = usersService.getFollowingUsers('2');

    expect(followingUsers.length).toBe(2);
    expect(followingUsers[0]).toBe(users[0]);
    expect(followingUsers[1]).toBe(users[2]);
  })

  test('get not following users', () => {
    const notFollowingUsers = usersService.getNotFollowingUsers('1');

    expect(notFollowingUsers.length).toBe(1);
    expect(notFollowingUsers[0]).toBe(users[2]);
  })

  test('does not get user itself for not following users', () => {
    const notFollowingUsers = usersService.getNotFollowingUsers('2');

    expect(notFollowingUsers.length).toBe(0);
  })

  test('get user timeline', () => {
    
    const mockTweets: Tweet[] = [];

    const spyGetUser = jest.spyOn(usersService, 'getUser');
    spyGetUser.mockReturnValue(users[1]);

    const spyGetFollowingUsers = jest.spyOn(usersService, 'getFollowingUsers');
    spyGetFollowingUsers.mockReturnValue([
      users[0],
      users[2],
    ]);

    const spyGetTweets = jest.spyOn(mockTweetsService, 'getTweets');
    spyGetTweets.mockReturnValue(mockTweets);
    
    expect(usersService.getTimeline('2')).toBe(mockTweets);
    expect(spyGetTweets).toHaveBeenCalledWith(['2', '5', '1', '3', '4']);
  })

  test('get user tweets', () => {
    const mockTweets: Tweet[] = [];

    const spyGetUser = jest.spyOn(usersService, 'getUser');
    spyGetUser.mockReturnValue(users[1]);

    const spyGetTweets = jest.spyOn(mockTweetsService, 'getTweets');
    spyGetTweets.mockReturnValue(mockTweets);

    expect(usersService.getUserTweets('2') ).toBe(mockTweets);
    expect(spyGetTweets).toHaveBeenCalledWith(users[1].tweets);
  })

  test('remove following', () => {
    usersService.removeFollowing('2', '1');

    expect(users[1].following.length).toBe(1);
    expect(users[1].following).not.toContain('1');
    expect(users[1].following).toContain('3');
  })

  test('follow user', () => {
    usersService.follow('3', '2');

    expect(users[2].following).toContain('2');
  })

  test('tweets to timeline', () => {
    usersService['_getTimeAgo'] = jest.fn().mockReturnValue('time');
    
    usersService.getUser = jest.fn().mockImplementation((id: string) => {
      const usersMock: any = {
        '1': { name: 'name 1'},
        '2': {name: 'name 2'}
      }

      return usersMock[id];
    });

    const tweets = [
      {
        id: '1',
        userId: '1',
        date: new Date(2022, 2, 1),
        text: 'Tweet 1'
      },
      {
        id: '2',
        userId: '2',
        date: new Date(2022, 2, 6, 18),
        text: 'Tweet 2'
      }
    ]

    const timeline = usersService.timelineToView(tweets);

    expect(timeline).toEqual([
      {
        userName: 'name 1',
        text: 'Tweet 1',
        timeAgo: 'time'
      },
      {
        userName: 'name 2',
        text: 'Tweet 2',
        timeAgo: 'time'
      }
    ]);
  })

  describe('_getTimeAgo function', () => {
    test('diff year', () => {

      const result = usersService['_getTimeAgo'](
        new Date(2022, 0, 1),
        new Date(2020, 0, 1),
        usersService['_months']
      )

      expect(result).toBe('January 2020');
    })

    test('diff month', () => {

      const result = usersService['_getTimeAgo'](
        new Date(2022, 1, 1),
        new Date(2022, 0, 1),
        usersService['_months']
      )

      expect(result).toBe('1 January');
    })

    test('diff day', () => {

      const result = usersService['_getTimeAgo'](
        new Date(2022, 0, 10),
        new Date(2022, 0, 1),
        usersService['_months']
      )

      expect(result).toBe('1 January');
    })

    test('diff hour', () => {

      const result = usersService['_getTimeAgo'](
        new Date(2022, 0, 1, 8),
        new Date(2022, 0, 1, 4),
        usersService['_months']
      )

      expect(result).toBe('4h');
    })

    test('same hour', () => {

      const result = usersService['_getTimeAgo'](
        new Date(2022, 0, 1, 4, 30),
        new Date(2022, 0, 1, 4, 10),
        usersService['_months']
      )

      expect(result).toBe('20min');
    })
  })

})