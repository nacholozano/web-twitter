# Environment settings

Node: v14.15.1
NPM: 6.14.8

# Run project

1. Checkout to `develop` branch
2. Go to `/twitter` folder
3. `npm i`
4. `npm start`
5. `npm test`
6. Open http://localhost:3000 on your browser